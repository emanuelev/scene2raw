CXX:=g++
CPPFLAGS=-g -std=c++11
IFLAGS= -I/home/ev314/software/boost_1_61_0/ 
LFLAGS= -L /home/ev314/software/boost_1_61_0/stage/lib -lboost_program_options

all: scene2raw scene2raw_tum

scene2raw: scene2raw.o lodepng.o
	$(CXX) -o scene2raw scene2raw.o lodepng.o

scene2raw.o: scene2raw.cpp scene2raw_shared.h 
	$(CXX) $(CPPFLAGS) -c scene2raw.cpp

scene2raw_tum: scene2raw_tum.o lodepng.o
	$(CXX) -o scene2raw_tum scene2raw_tum.o lodepng.o $(LFLAGS) 

scene2raw_tum.o: scene2raw_tum.cpp scene2raw_shared.h 
	$(CXX) $(CPPFLAGS) $(IFLAGS) -c scene2raw_tum.cpp

lodepng.o: lodepng.cpp lodepng.h
	$(CXX) -c lodepng.cpp

clean:
	rm *.o

.PHONY: all clean
