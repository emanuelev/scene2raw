/*
 * Convert a TUM RGB-D dataset to a raw file of use within SLAMBench.
 *
 */
#include "scene2raw_tum.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <cmath>
#include <string>
#include <iomanip>

#include <boost/program_options.hpp>
#include <boost/regex.hpp>
#include <boost/assign.hpp>

#include "lodepng.h"
#include "scene2raw_shared.h"

namespace po = boost::program_options;

/*
 * timestamp tx ty tz qx qy qz qw
 */
struct GroundTruthRecord
{
    GroundTruthRecord() {};
    GroundTruthRecord(double tx, double ty, double tz, double qx, double qy, double qz, double qw) : tx(tx), ty(ty),
                                                                                                     tz(tz), qx(qx),
                                                                                                     qy(qy), qz(qz),
                                                                                                     qw(qw) { }

    double tx, ty, tz, qx, qy, qz, qw;
};

struct TimeStampAssociations
{
    TimeStampAssociations(std::string &rgbImagePath, std::string &depthImagePath) : rgbImagePath(rgbImagePath),
                                                                                    depthImagePath(depthImagePath) { }

    std::string rgbImagePath;
    std::string depthImagePath;
};

typedef double TimeStamp;

typedef std::map<TimeStamp, std::string> TimeStampDataMap;
typedef std::map<TimeStamp, TimeStampAssociations> TimeStampAssociationMap;
typedef std::map<TimeStamp, GroundTruthRecord> GroundTruthDataMap;


// Parameters picked up from commandline, used everywhere.
std::string dataFolder;         //Folder where TUM dataset was extracted.
double maxDiff;                 //Maximum difference beween two times (same as in associate.py)
int tumType;

std::string outputFilePath;     //Path to output file.
std::string outputTimingAssocFilePath;


/*
 * Parse the commandline arguments.
 */
void parseArgs(int argc, char **argv)
{
    po::options_description desc("scene2raw-tum options");
    desc.add_options()
            ("help", "produce help message")

            //Required
            ("folder", po::value<std::string>(&dataFolder)->required(), "Path to folder where TUM dataset was extracted")
            ("raw-output", po::value<std::string>(&outputFilePath)->required(), "Path to folder where TUM dataset was extracted")
            ("timing-assoc-output", po::value<std::string>(&outputTimingAssocFilePath)->required(), "Path to file for timing association data")
            ("tum-type", po::value<int>(&tumType)->required(), "TUM dataset type [1,2,3]")

            //Optional
            ("maxdiff", po::value<double>(&maxDiff)->default_value(0.02), "the maximum difference between two timestamps");
    try
    {
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")  || (tumType != 1 && tumType != 2 && tumType != 3))
        {
            std::cout << desc << "\n";

            exit(0);
        }
    }
    catch(std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;

        std::cout << desc << std::endl;
        exit(1);
    }
    catch(...)
    {
        std::cerr << "Unknown error!" << "\n";
        exit(1);
    }
}

//http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

TimeStampDataMap parseTimestampMapperFile(const std::string &path)
{
    TimeStampDataMap map;

    std::ifstream file;
    file.open(path.c_str());

    if (!file.is_open())
    {
        std::cerr << "Failed to open: " << path << " for reading" << std::endl;
        exit(1);
    }

    std::string line;
    while (getline(file,line))
    {
        std::vector<std::string> parts = split(line, ' ');

        if (parts.size() != 2)
        {
            continue;
        }

        map.insert(std::pair<double, std::string>(std::stod(parts[0]), parts[1]));
    }


    file.close();

    return map;
}

GroundTruthDataMap parseGroundTruthFile(const std::string &path)
{
    GroundTruthDataMap map;

    std::ifstream file;
    file.open(path.c_str());

    if (!file.is_open())
    {
        std::cerr << "Failed to open: " << path << " for reading" << std::endl;
        exit(1);
    }

    std::string line;
    while (getline(file,line))
    {
        std::vector<std::string> parts = split(line, ' ');

        if (parts.size() != 8)
        {
            continue;
        }

        double timestamp = std::stod(parts[0]);

        GroundTruthRecord rec(std::stod(parts[1]), std::stod(parts[2]), std::stod(parts[3]),
                              std::stod(parts[4]), std::stod(parts[5]), std::stod(parts[6]), std::stod(parts[7]));

        map.insert(std::pair<double, GroundTruthRecord>(timestamp, rec));
    }

    file.close();

    return map;
}

template<typename T>
std::pair<bool, T>  getClosest(std::map<TimeStamp, T> map, TimeStamp val, TimeStamp offset, TimeStamp maxDiff)
{
    double closestDiff = maxDiff;
    bool hasPair = false;
    T closest;

    for (auto i : map)
    {
        double diff = fabs(val - (i.first + offset));
        if (diff < closestDiff)
        {
            hasPair = true;
            closest = i.second;
            closestDiff = diff;
        }
    }

    return std::pair<TimeStamp, T>(hasPair, closest);
}


/*
 * Find the best pairing of rgb and depth images, with maxDiff.
 */
TimeStampAssociationMap associate(TimeStampDataMap &rgbImages, TimeStampDataMap &depths, double maxDiff)
{
    TimeStampAssociationMap associations;
    double offset = 0;

    for (auto rgbImage : rgbImages)
    {
        std::pair<bool, std::string> depthClosest = getClosest<std::string>(depths, rgbImage.first, offset, maxDiff);

        if (!depthClosest.first)
        {
            std::cout.setf(std::ios::fixed, std::ios::floatfield);
            std::cout << "Failed to find depth image for: " << rgbImage.first << std::endl;
            continue;
        }

        TimeStampAssociations assoc(rgbImage.second, depthClosest.second);
        associations.insert(std::pair<TimeStamp, TimeStampAssociations>(rgbImage.first, assoc));
    }

    return associations;
}

/*
 * Writeout the raw file, using (ordered) pairings of depth and rgb frames.
 */
void writeRaw(TimeStampAssociationMap pairings, const std::string &outputFilePath)
{
    FILE* outfile = fopen(outputFilePath.c_str(), "wb");
    if (!outfile)
    {
        std::cerr << "Failed to open: " << outputFilePath << " - the raw file" << std::endl;
        exit(1);
    }

    uint2 inputSize = make_uint2(640, 480);
    static const uint2 imageSize = { 320, 240 };

    uchar3 *rgbImage = (uchar3*) malloc(sizeof(uchar3) * inputSize.x * inputSize.y);
    ushort *depthImage = (ushort*) malloc(sizeof(ushort) * inputSize.x * inputSize.y);

    int i = 0;
    for (auto pair : pairings)
    {
        TimeStampAssociations &assoc = pair.second;
        std::string rgbFilePath = dataFolder + "/" + assoc.rgbImagePath;
        std::string depthFilePath = dataFolder + "/" + assoc.depthImagePath;

        unsigned error;
        uchar4* image;
        unsigned width = 640, height = 480;


        /*
         * 16bit monochrome image, scaled by 5000.
         * lodepng monochome: all x,y,z values are the same
         */
        std::vector<unsigned char> raw_image;
        std::vector<unsigned char> png;
        lodepng::State state = lodepng::State();
        state.decoder.color_convert = false;

        lodepng::load_file(raw_image, depthFilePath); //load the image file with given filename
        unsigned e = lodepng::decode(png, width, height, state, raw_image);


        //Extract the 16bit value, bigendian format (png method for multibyte)
        // Divide by 5: 5000 units = 1m (scene2raw does *1000).
        int k = 0;
        for(unsigned y = 0; y < height; y++) {
            for (unsigned x = 0; x < width; x++) {
                size_t index = y * width * 2 + x * 2;

                int r = png[index + 0] * 256 + png[index + 1];
                depthImage[k++] = r / 5;
            }
        }

        //RGB Image
        error = lodepng_decode32_file((unsigned char**) &image, &width, &height, rgbFilePath.c_str());
        if (error)
        {
            std::cerr << "Error: " << error << " " << lodepng_error_text(error) << std::endl;
        }
        else
        {
            for (unsigned int i = 0; i < inputSize.y * inputSize.x; i++) {
                rgbImage[i] = make_uchar3(image[i].x, image[i].y, image[i].z);
            }
        }

        //Write out images and repsective sizes.
        int c = 0;
        c += fwrite(&(inputSize), sizeof(imageSize), 1, outfile);
        c += fwrite(depthImage, sizeof(uint16_t), inputSize.x * inputSize.y, outfile);
        c += fwrite(&(inputSize), sizeof(imageSize), 1, outfile);
        c += fwrite(rgbImage, sizeof(uchar3), inputSize.x * inputSize.y, outfile);

        if (c != 2 * (1 + inputSize.x * inputSize.y))
            std::cout << "Writing failed at: " << i << std::endl;

        std::cout << "\rWrite frame " << std::setw(10) << i << " ";

        if (i % 2) {
            fflush(stdout);
        }

        ++i;
        free(image);
    }

    free(rgbImage);
    free(depthImage);

    std::cout << std::endl;

    fclose(outfile);
}

/*
 * Write ground truth file in TUM format.
 */
void writeTimingAssocFile(TimeStampAssociationMap pairings, const std::string &outputTimingAssocFilePath)
{
    std::ofstream timingAssocFile(outputTimingAssocFilePath, std::ios::out);

    if (!timingAssocFile.is_open())
    {
        std::cerr << "Failed to open timings association output file: " << outputTimingAssocFilePath << std::endl;
    }

    timingAssocFile.setf(std::ios_base::fixed, std::ios::floatfield);
    timingAssocFile.precision(6);

    for (auto pair : pairings)
    {
        timingAssocFile << pair.first << std::endl;
    }

    timingAssocFile.close();
}

int main(int argc, char **argv)
{
    parseArgs(argc, argv);

    TimeStampDataMap depths = parseTimestampMapperFile(dataFolder + "/depth.txt");
    TimeStampDataMap rgbImages = parseTimestampMapperFile(dataFolder + "/rgb.txt");

    if (depths.size() == 0)
    {
        std::cerr << "No depth images, cannot continue. " << std::endl;
        return -1;
    }

    if (rgbImages.size() == 0)
    {
        std::cerr << "No rgb images, cannot continue. " << std::endl;
        return -1;
    }

    TimeStampAssociationMap associatedData = associate(rgbImages, depths, maxDiff);

    writeRaw(associatedData, outputFilePath);
    writeTimingAssocFile(associatedData, outputTimingAssocFilePath);

    GroundTruthDataMap groundTruthDataMap = parseGroundTruthFile(dataFolder + "/groundtruth.txt");
    std::pair<bool, GroundTruthRecord> groundTruthClosest = getClosest<GroundTruthRecord>(groundTruthDataMap, associatedData.begin()->first, 0.0, maxDiff);

    if (!groundTruthClosest.first)
    {
        std::cerr << "Failed to find ground truth position for first RGB image." << std::endl;
    }

    GroundTruthRecord rec = groundTruthClosest.second;

    std::cout.setf(std::ios_base::fixed, std::ios::floatfield);
    std::cout.precision(4);
    std::cout << "Starting Rotation (quaterion): " << rec.qx << rec.qy << rec.qz << rec.qw << std::endl;

    return 0;
}
