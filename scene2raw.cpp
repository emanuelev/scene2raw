/*

 Copyright (c) 2014 University of Edinburgh, Imperial College, University of Manchester.
 Developed in the PAMELA project, EPSRC Programme Grant EP/K008730/1

 This code is licensed under the MIT License.

 */

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <stdint.h>
#include <vector>
#include <sstream>
#include <cstring>
#include <cmath>
#include <sstream>
#include <iomanip>


#include "scene2raw_shared.h"
#include "lodepng.h"


static const float SceneK[3][3] = { 481.20, 0.00, 319.50,
									0.00, -480.00, 239.50,
									0.00, 0.00, 1.00 };

static const int _scenewidth = 640;
static const int _sceneheight = 480;

static const CameraCalibration _cameraCalibration(SceneK[0][2], SceneK[1][2], SceneK[0][0], SceneK[1][1]);

int readDepthFile(ushort * depthMap, const char * filename)
{
	std::ifstream source;
	source.open(filename, std::ios_base::in);

	if (!source)
	{
		std::cerr << "Can't open Data from " << filename << " !\n";

		return -1;
	}

	int index = 0;
	while (source.good())
	{
		double d;
		source >> d;
		if (640 * 480 <= index)
			continue; // FIXME : not sure why it read one more float
		depthMap[index] = d * 1000;

		index++;
	}

	return index;
}

int main(int argc, char ** argv) {

	uint2 inputSize = make_uint2(640, 480);
	static const uint2 imageSize = { 320, 240 };

	if (argc != 3) {
		std::cout
				<< "Bad entries... I just need the scene directory and the output file"
				<< std::endl;
		exit(1);
	}

	// Input
	// wait for something like this : scene/ ( scene_00_0000.depth )
	std::string dir = argv[1];
	FILE* pFile = fopen(argv[2], "wb");
	if (!pFile) {
		std::cout << "File opening failed : " << argv[2] << std::endl;
		exit(1);
	}

	uchar3 * rgbImage = (uchar3*) malloc(
			sizeof(uchar3) * inputSize.x * inputSize.y);

	ushort * inputFile = (ushort*) malloc(
			sizeof(ushort) * inputSize.x * inputSize.y);

	for (int i = 0; true; i++)
	{
		unsigned error;
		uchar4* image;
		unsigned width, height;

		std::ostringstream filename;
		std::ostringstream rgbfilename;

		filename << dir << "/scene_00_" << std::setfill('0') << std::setw(4)
				<< i << ".depth";
		rgbfilename << dir << "/scene_00_" << std::setfill('0') << std::setw(4)
				<< i << ".png";

		if (readDepthFile(inputFile, filename.str().c_str()) == -1)
		{
			break;
		}

		distortDepthFrame(inputFile, _sceneheight, _scenewidth, _cameraCalibration);

		error = lodepng_decode32_file((unsigned char**) &image, &width, &height,
				rgbfilename.str().c_str());
		if (error) {
			printf("error %u: %s\n", error, lodepng_error_text(error));
		} else {
			for (unsigned int i = 0; i < inputSize.y * inputSize.x; i++) {
				rgbImage[i] = make_uchar3(image[i].x, image[i].y, image[i].z);
			}
		}

		int total = 0;
		total += fwrite(&(inputSize), sizeof(imageSize), 1, pFile);
		total += fwrite(inputFile, sizeof(uint16_t), inputSize.x * inputSize.y, pFile);
		total += fwrite(&(inputSize), sizeof(imageSize), 1, pFile);
		total += fwrite(rgbImage, sizeof(uchar3), inputSize.x * inputSize.y, pFile);

		std::cout << "\rRead frame " << std::setw(10) << i << " ";

		if (i % 2)
		{
			fflush(stdout);
		}
		free(image);
	}
	std::cout << std::endl;
	fclose(pFile);
}



