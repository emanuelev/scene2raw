# SLAMBench 1.0 Dataset generation tool

`scene2raw` and `scene2raw_tum` are self-contained tools that generate
SLAMBench compatible raw files for ICL-NUIM and TUM respectively.

## Usage

scene2raw /path/to/depth/data/ /path/to/output/file.raw

scene2raw_tum command options:
```
--help                    produce help message
--folder arg              Path to folder where TUM dataset was extracted
--raw-output arg          Output raw file filename
--timing-assoc-output arg Output file for timing association data
--tum-type arg            TUM dataset type [1,2,3]
--maxdiff arg (=0.02)     the maximum difference between two timestamps
```
